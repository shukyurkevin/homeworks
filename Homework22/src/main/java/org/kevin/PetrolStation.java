package org.kevin;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import lombok.Getter;

@Getter
public class PetrolStation {

  private volatile int amount;
  private Semaphore semaphore;
  private volatile int availableAmount;

  Random rand = new Random();
  public PetrolStation(int amount) {
    this.amount = amount;
    this.semaphore = new Semaphore(3);
  }

  public void doRefuel(int value) throws InterruptedException {
    int randomTime = rand.nextInt(7001)+3000;
    semaphore.acquire();
    try {
      if (availableAmount < value) {
        System.out.println("Not enough fuel");
        return;
      }
        System.out.println("Refueling started on" + Thread.currentThread().getName());
        availableAmount -= value;
        Thread.sleep(randomTime);
        amount -= value;
        System.out.println("Refueling finished, remaining fuel: " + amount);
    } finally {
        semaphore.release();
    }
  }

}