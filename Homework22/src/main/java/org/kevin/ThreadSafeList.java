package org.kevin;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ThreadSafeList<T> {

  private List<T> list = new CopyOnWriteArrayList<>();

  public void add(T value) {
    list.add(value);
  }

  public void remove(T value) {
    list.remove(value);
  }

  public T get(int index) {
    return list.get(index);
  }
}