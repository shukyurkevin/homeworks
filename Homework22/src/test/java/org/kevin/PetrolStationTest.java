package org.kevin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PetrolStationTest {
    @Test
    public void doRefuelTest() throws InterruptedException {
      PetrolStation station = new PetrolStation(1000);
      Thread car1 = new Thread(() -> {
        try {
          station.doRefuel(200);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      });

      Thread car2 = new Thread(() -> {
        try {
          station.doRefuel(300);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      });

      Thread car3 = new Thread(() -> {
        try {
          station.doRefuel(250);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      });
      car1.start();
      car2.start();
      car3.start();

      car1.join();
      car2.join();
      car3.join();
      Assertions.assertEquals(250, station.getAmount());
    }
}
