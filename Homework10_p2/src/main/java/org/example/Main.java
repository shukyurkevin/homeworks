package org.example;

import org.kevin.MathLib;

public class Main {
  public static void main(String[] args) {
    System.out.println(MathLib.mult(3, 7));
    System.out.println(MathLib.divide(10, 2));
    System.out.println(MathLib.pow(3, 4));
    System.out.println(MathLib.sum(2, 5));
    System.out.println(MathLib.sqrt(9));
    System.out.println(MathLib.substract(10, 9));
    System.out.println(MathLib.module(-49));
  }
}