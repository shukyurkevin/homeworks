package org.kevin;
import java.util.Arrays;

public class ValueCalculator {

  private float[] arr;
  private int size;
  private int half;

  public ValueCalculator(int size) {
    this.size = size;
    this.half = size / 2;
    this.arr = new float[size];
  }

  public String calculate() {
    Arrays.fill(arr, 1.0f);

    float[] a1 = new float[half];
    float[] a2 = new float[half];

    System.arraycopy(arr, 0 , a1, 0, half);
    System.arraycopy(arr, half, a2, 0, half);

    long start = System.currentTimeMillis();

    Thread t1 = new Thread(() -> calculateValues(a1));
    Thread t2 = new Thread(() -> calculateValues(a2));

    t1.start();
    t2.start();

    try {
      t1.join();
      t2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.arraycopy(a1, 0, arr, 0, half);
    System.arraycopy(a2, 0, arr, half, half);

    long end = System.currentTimeMillis();
    return "Time passed: " + (end - start) + " ms";
  }
  private void calculateValues(float[] array) {
    for (int i = 0; i < array.length; i++) {
      array[i] *= Math.sin(0.2f + i / 5.0) * Math.cos(0.2f + i / 5.0) * Math.cos(0.4f + i / 2.0);
    }
  }
}