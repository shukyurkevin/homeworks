package org.kevin.enums;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import lombok.SneakyThrows;

public enum DataBaseConnection {
  INSTANCE;
  private final HikariConfig config;
  private final HikariDataSource dataSource;

  DataBaseConnection() {
    config = new HikariConfig();
    config.setJdbcUrl("jdbc:mysql://localhost/kevin");
    config.setUsername("kevin");
    config.setPassword("password");
    dataSource = new HikariDataSource(config);
  }

  @SneakyThrows
  public Connection getConnection(){
    return dataSource.getConnection();
  }
  @SneakyThrows
  public void closeConnection(){
    dataSource.close();
  }
}
