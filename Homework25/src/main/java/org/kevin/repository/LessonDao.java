package org.kevin.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.kevin.entity.Lesson;
import org.kevin.enums.DataBaseConnection;

@Setter
@Getter
public class LessonDao {
  @SneakyThrows
  public void addLesson(Lesson lesson) {
    String insert = "INSERT INTO Lesson(name, updatedAt, homework_id)" +
        "  values(?, NOW(), ?)";
    try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
         PreparedStatement statement = connection.prepareStatement(insert)
    ){
      statement.setString(1, lesson.getName());
      statement.setLong(2, lesson.getHomework_id());
      statement.execute();
    }
  }
  @SneakyThrows
  public void deleteLesson(String lessonName){
    String delete = "DELETE FROM Lesson WHERE name = ?";
    try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
        PreparedStatement statement = connection.prepareStatement(delete)){
      statement.setString(1,lessonName);
      statement.execute();
    }
  }
  @SneakyThrows
  public void deleteLesson(long lessonId){
    String delete = "DELETE FROM Lesson WHERE id = ?";
    try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
         PreparedStatement statement = connection.prepareStatement(delete)){
      statement.setLong(1,lessonId);
      statement.execute();
    }
  }
  @SneakyThrows
  public void getAll(){
    String query = "SELECT * FROM lesson";
    try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
        Statement statement = connection.createStatement()
    ){
      ResultSet resultSet = statement.executeQuery(query);
      while (resultSet.next()){
        System.out.println("ID: "+ resultSet.getLong("id"));
        System.out.println("NAME: " + resultSet.getString("name"));
        System.out.println("UPDATED_TIME: " + resultSet.getTimestamp("updatedAt"));
        System.out.println("HOMEWORK_ID: "+ resultSet.getLong("homework_id"));
      }
    }
  }
  @SneakyThrows
  public void getById(long id){
    String query = "SELECT * FROM lesson Where id = ?";
    try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
    PreparedStatement statement = connection.prepareStatement(query)
    ){
      statement.setLong(1,id);
      ResultSet resultSet = statement.executeQuery();
      while (resultSet.next()){
        System.out.println("ID: "+ resultSet.getLong("id"));
        System.out.println("NAME: " + resultSet.getString("name"));
        System.out.println("UPDATED_TIME: " + resultSet.getTimestamp("updatedAt"));
        System.out.println("HOMEWORK_ID: "+ resultSet.getLong("homework_id"));
      }
    }
  }
}
