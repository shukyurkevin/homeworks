package org.kevin.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.kevin.entity.Homework;
import org.kevin.enums.DataBaseConnection;

@Getter
@Setter
public class HomeworkDao {

@SneakyThrows
  public void addHomework(Homework homework) {
    String insert = "INSERT INTO Homework(name, description)" +
        "  values(?, ?)";
   try (Connection connection = DataBaseConnection.INSTANCE.getConnection();
       PreparedStatement statement = connection.prepareStatement(insert)
   ){
     statement.setString(1, homework.getName());
     statement.setString(2, homework.getDescription());
     statement.execute();
   }
   }
  }
