package org.kevin.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Homework {
  private long id;
  private String name;
  private String description;
}
