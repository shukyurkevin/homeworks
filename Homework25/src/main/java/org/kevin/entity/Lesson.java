package org.kevin.entity;

import java.sql.Timestamp;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Lesson {
  private long id;
  private String name;
  private Timestamp timestamp;
  private long homework_id;
}
