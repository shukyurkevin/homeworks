package org.kevin;

import org.junit.jupiter.api.Test;
import org.kevin.entity.Lesson;
import org.kevin.repository.LessonDao;

public class LessonDaoTest {
  LessonDao lessonDao = new LessonDao();
  @Test
  void testQuery(){
    lessonDao.getAll();
  }
  @Test
  void addLessonTest(){
    Lesson lesson3 = Lesson.builder()
        .name("lesson3")
        .homework_id(2)
        .build();
    lessonDao.addLesson(lesson3);
  }
  @Test
  void deleteLessonByIdTest(){
    lessonDao.deleteLesson(3);
  }
  @Test
  void deleteLessonByName(){
    lessonDao.deleteLesson("lesson3");
  }
  @Test
  void getLessonByIdTest(){
    lessonDao.getById(3);
  }
}
