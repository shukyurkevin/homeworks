package org.kevin;

import org.junit.jupiter.api.Test;
import org.kevin.entity.Student;
import org.kevin.repository.StudentDao;

public class StudentDaoTest {
  private final StudentDao studentDao = new StudentDao();
  @Test
  void test(){
    studentDao.saveStudent(Student.builder()
             .name("second")
             .email("second@gmail.com")
        .build());
  }
  @Test
  void deleteTest(){
    studentDao.delete(1L);
  }
  @Test
  void updateTest(){
    Student student= studentDao.getByID(2);
    student.setName("new name");
    studentDao.update(student);
  }
}
