package org.kevin.service;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.kevin.entity.Student;

public class HibernateSession {
  private static SessionFactory sessionFactory;

  static {
    Configuration configuration = new Configuration();

    configuration.configure("hibernate.cfg.xml");
    configuration.addAnnotatedClass(Student.class);

    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
        .applySettings(configuration.getProperties()).build();
    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
  }


  public static SessionFactory getSessionFactory(){
    return sessionFactory;
  }
}