package org.kevin.repository;


import java.util.List;
import lombok.SneakyThrows;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;
import org.hibernate.query.criteria.JpaCriteriaQuery;
import org.kevin.entity.Student;
import org.kevin.service.HibernateSession;


public class StudentDao {

  Transaction transaction = null;
  @SneakyThrows
  public void saveStudent(Student student) {

    try (Session session = HibernateSession.getSessionFactory().openSession()) {
      transaction = session.beginTransaction();
      session.persist(student);
      transaction.commit();
    }
  }
  @SneakyThrows
  public Student getByID(long id) {

    try (Session session = HibernateSession.getSessionFactory().openSession()) {
      return session.get(Student.class, id);
    }
  }
  @SneakyThrows
  public List<Student> getAll() {

    try (Session session = HibernateSession.getSessionFactory().openSession()) {
      HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
      JpaCriteriaQuery<Student> query = cb.createQuery(Student.class);
      query.from(Student.class);
      return session.createQuery(query).getResultList();
    }
  }

  @SneakyThrows
  public void update(Student student) {

    try (Session session = HibernateSession.getSessionFactory().openSession()) {
      transaction = session.beginTransaction();
      session.merge(student);
      transaction.commit();
    }
  }
  @SneakyThrows
  public void delete(Long id) {

    try (Session session = HibernateSession.getSessionFactory().openSession()) {
      transaction = session.beginTransaction();
      session.remove(session.get(Student.class,id));
      transaction.commit();
    }
  }

}