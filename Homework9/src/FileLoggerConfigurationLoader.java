import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileLoggerConfigurationLoader extends ALoggerConfigurationLoader {
  public FileLoggerConfiguration load(String filePath) {
    FileLoggerConfiguration config = new FileLoggerConfiguration();
    try (BufferedReader read = new BufferedReader(new FileReader(filePath))) {
      String str = read.readLine();
      while (str != null) {
        String[] splitStr = str.split(":");
        if (splitStr.length == 2) {
          String key = splitStr[0].trim();
          String value = splitStr[1].trim();
          switch (key) {
            case "FILE" -> config.setPath(value);
            case "LEVEL" -> config.setLevel(LogginLevel.valueOf(value));
            case "MAX-SIZE" -> config.setMaxSize(Integer.parseInt(value));
            case "FORMAT" -> config.setFormat(value);
          }
        }
        str = read.readLine();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return config;
  }
}
