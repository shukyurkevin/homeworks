public class FileLoggerConfiguration extends ALoggerConfiguration {
  public String path;
  public LogginLevel level;
  public int maxSize;
  public String format;

  public void setPath(String path) {
    this.path = path;
  }

  public void setLevel(LogginLevel level) {
    this.level = level;
  }

  public void setMaxSize(int maxSize) {
    this.maxSize = maxSize;
  }

  public void setFormat(String format) {
    this.format = format;
  }
}

