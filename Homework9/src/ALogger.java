import java.io.IOException;

public abstract class ALogger {
  ALoggerConfiguration aLoggerConfiguration;

  public ALogger(ALoggerConfiguration aLoggerConfiguration) {
    this.aLoggerConfiguration = aLoggerConfiguration;
  }

  public abstract void debug(String msg) throws IOException;

  public abstract void info(String msg) throws IOException;
}

