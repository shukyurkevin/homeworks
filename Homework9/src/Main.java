import java.io.IOException;

public class Main {

  public static void main(String[] args) {

    try {
      int i = 0;
      String path = "src\\loader.yaml";
      ALoggerConfigurationLoader fileLoggerConfigurationLoader =
          new FileLoggerConfigurationLoader();
      FileLoggerConfiguration config =
          (FileLoggerConfiguration) fileLoggerConfigurationLoader.load(path);
      ALogger fileLogger = new FileLogger(config);
      while (i++ < 20) {
        fileLogger.debug("someDebug");
        fileLogger.info("someInfo");
      }
      config.setLevel(LogginLevel.INFO);
      while (i++ < 30) {
        fileLogger.info("someInfo");
        fileLogger.debug("someDebug");
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

  }

}