import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileLogger extends ALogger {
  FileLoggerConfiguration fileLoggerConfiguration;
  File file;
  SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss a");
  SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("dd_MM_yyyy-hh_mm_ss_ms");

  public FileLogger(FileLoggerConfiguration fileLoggerConfiguration) {
    super(fileLoggerConfiguration);
    this.fileLoggerConfiguration = fileLoggerConfiguration;
    this.file = new File(fileLoggerConfiguration.path);
  }


  @Override
  public void debug(String msg) throws IOException {
    logMessage(LogginLevel.DEBUG, msg);
  }

  public void info(String msg) throws IOException {
    logMessage(LogginLevel.INFO, msg);
  }

  private void logMessage(LogginLevel level, String msg) throws IOException {
    if (fileLoggerConfiguration.level.ordinal() >= level.ordinal()) {
      try {
        if (file.length() < fileLoggerConfiguration.maxSize) {
          FileWriter fileWriter = new FileWriter(file, true);
          fileWriter.write("[" + simpleDateFormat.format(new Date()) + "]" + "[" + level + "]" +
              "notification: " + msg + "\n");
          fileWriter.close();
        } else {
          throw new FileMaxSizeReachedException("size now: " + file.length() + " his max size: "
              + fileLoggerConfiguration.maxSize + " path to file: " + file);
        }
      } catch (FileMaxSizeReachedException ex) {
        file = new File(
            fileLoggerConfiguration.path + "LOG_" + simpleTimeFormat.format(new Date()) +
                fileLoggerConfiguration.format);
      }
    }
  }
}