package org.kevin;

import java.lang.reflect.InvocationTargetException;

public class Main {
  public static void main(String[] args) {
    try {
      TestRunner.start(TestRunnerTest.class);
    } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
      e.printStackTrace();
    } catch (NoSuchMethodException e) {
      throw new RuntimeException(e);
    }
  }
}