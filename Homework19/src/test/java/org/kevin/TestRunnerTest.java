package org.kevin;

import org.kevin.annotations.AfterSuite;
import org.kevin.annotations.BeforeSuite;
import org.kevin.annotations.Test;

public class TestRunnerTest {
  @BeforeSuite
  public void setUp() {
    System.out.println("setUp");
  }

  @Test(priority = 2)
  public void test1() {
    System.out.println("Test1");
  }

  @Test(priority = 1)
  public void test2() {
    System.out.println("Test2");
  }

  @AfterSuite
  public void AfterTest() {
    System.out.println("AfterSuite Test");
  }
}
