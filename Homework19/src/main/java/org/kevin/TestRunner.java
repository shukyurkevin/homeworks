package org.kevin;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import org.kevin.annotations.AfterSuite;
import org.kevin.annotations.BeforeSuite;
import org.kevin.annotations.Test;

public class TestRunner {
  public static void start(Class<?> testClass)
      throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
    Object instance = testClass.getConstructor().newInstance();
    List<Method> methods = Arrays.asList(testClass.getMethods());

    long beforeSuiteMethodsCount = methods.stream().filter(m -> m.isAnnotationPresent(BeforeSuite.class)).count();
    if (beforeSuiteMethodsCount > 1){
      throw new RuntimeException("More than 1 BeforeSuiteMethods");
    }
    Method beforeSuiteMethod = methods.stream().filter(m -> m.isAnnotationPresent(BeforeSuite.class)).findFirst().orElse(null);
    if (beforeSuiteMethod != null) {
      beforeSuiteMethod.invoke(instance);
    }

    List<Method> testMethods = methods.stream().filter(m -> m.isAnnotationPresent(Test.class))
        .sorted(Comparator.comparingInt(m -> m.getAnnotation(Test.class).priority())).toList();
    for (Method testMethod : testMethods) {
      testMethod.invoke(instance);
    }

    long afterSuiteMethodsCount = methods.stream().filter(m -> m.isAnnotationPresent(BeforeSuite.class)).count();
    if (afterSuiteMethodsCount > 1){
      throw new RuntimeException("More than 1 AfterSuiteMethods");
    }
    Method afterSuiteMethod = methods.stream().filter(m -> m.isAnnotationPresent(AfterSuite.class)).findFirst().orElse(null);
    if (afterSuiteMethod != null) {
      afterSuiteMethod.invoke(instance);
    }
  }
}