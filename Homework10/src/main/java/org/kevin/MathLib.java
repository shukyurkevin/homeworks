package org.kevin;

public class MathLib {
  public static double sum(double a, double b) {
    return a + b;
  }

  public static double substract(double a, double b) {
    return a - b;
  }

  public static double mult(double a, double b) {
    return a * b;
  }

  public static double divide(double a, double b) {
    return a / b;
  }

  public static double pow(double a, double b) {
    return Math.pow(a, b);
  }

  public static double sqrt(double a) {
    return Math.sqrt(a);
  }
  public static double module(double a){
    return Math.abs(a);
  }
}
