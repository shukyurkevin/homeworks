package org.kevin;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductService {
  private List<Product> productList;
  public ProductService(List<Product> products){
    this.productList = new ArrayList<>();
  }

  public void addProduct(Product product){
    this.productList.add(product);
  }

  public List<Product> getFilteredBy250(double minPrice, String productType){
    return productList.stream()
        .filter(product -> product.getType().equals(productType) && product.getPrice() > minPrice)
        .collect(Collectors.toList());
  }
  public List<Product> getDiscountedProducts() {
    return productList.stream()
        .filter(product -> product.getType().equals("Book") && product.canDiscount)
        .collect(Collectors.toList());
  }
  public Optional<Product> getCheapestBook() {
    return productList.stream()
        .filter(product -> product.getType().equals("Book"))
        .min(Comparator.comparing(Product::getPrice));
  }
  public List<Product> getLastAddedProducts(long limit) {
    return productList.stream()
        .sorted(Comparator.comparing(Product::getCreateTime, Comparator.reverseOrder()))
        .limit(limit)
        .collect(Collectors.toList());
  }
  public double getTotalValueOfFilteredProducts(String productType, double minPrice) {
    return productList.stream()
        .filter(product -> product.getType().equals(productType))
        .filter(product -> product.getPrice() <= minPrice)
        .filter(product -> product.createTime.getYear() == LocalDate.now().getYear())
        .mapToDouble(Product::getPrice)
        .sum();
  }
  public Map<String, List<Product>> groupProductsByType() {
    return productList.stream()
        .collect(Collectors.groupingBy(Product::getType));
  }
}
