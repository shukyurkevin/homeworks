package org.kevin.phonebook;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Record {
  private String name;
  private int phoneNumber;
}
