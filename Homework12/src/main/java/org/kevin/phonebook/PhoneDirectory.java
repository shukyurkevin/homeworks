package org.kevin.phonebook;

import java.util.ArrayList;
import java.util.List;

public class PhoneDirectory {
  public List<Record> recordList = new ArrayList<>();
  public void add(String name,int phoneNumber){
    recordList.add(new Record(name,phoneNumber));
  }
  public Record find(String name){
    for (Record record: recordList){
      if (record.getName().equals(name)){
        return record;
      }
    }
    return null;
  }
  public List<Record> findAll(String name){
    List<Record> recordsFound = new ArrayList<>();
    for (Record record: recordList){
      if (record.getName().equals(name)){
        recordsFound.add(record);
      }
    }
  if (!recordsFound.isEmpty()){
    return recordsFound;
  }
  return null;
  }
}
