package org.kevin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

public class ListMethods {

  public int countOccurrence(List<String> list, String s){
    int count = 0;
    for (String list1: list) {
      if (list1.equals(s))count++;
    }
    return count;
  }
  public List<Integer> toList(int[] arrays){
    List<Integer> newList = new ArrayList<>();
    for (int i = 0;i<arrays.length;i++){
      newList.add(i,arrays[i]);
    }
    return newList;
  }
  public List<Integer> findUnique(List<Integer> numberList){
    List<Integer> uniqueList = new ArrayList<>();
    for (int list: numberList){
      if (!uniqueList.contains(list)){
        uniqueList.add(list);
      }
    }
    return uniqueList;
  }
  public void calcOccurrence(List<String> list){
    Map<String,Integer> map = new HashMap<>();
    for (String s:list){
      if (!map.containsKey(s)){
        map.put(s,1);
      }else {
        int i = map.get(s);
        map.put(s,i+1);
      }
    }
    for (Map.Entry<String,Integer> mapEntry: map.entrySet()){
      System.out.println(mapEntry.getKey()+": "+mapEntry.getValue());
    }
  }
  public List<Counter> findOccurrence(List<String> list){
    List<Counter> readyList = new ArrayList<>();
    Map<String,Integer> map = new HashMap<>();
    for (String s:list){
      if (!map.containsKey(s)){
        map.put(s,1);
      }else {
        int i = map.get(s);
        map.put(s,i+1);
      }
    }
    for (Map.Entry<String,Integer> mapEntry: map.entrySet()){
      readyList.add(new Counter(mapEntry.getKey(), mapEntry.getValue()));
    }
    return readyList;
  }
}
@Builder
@Getter
class Counter{
  String name;
  int occurrence;

}
