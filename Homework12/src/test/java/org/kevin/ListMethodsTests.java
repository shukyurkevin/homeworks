package org.kevin;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ListMethodsTests {
  private final ListMethods listMethods = new ListMethods();
   @Test
  public void countOccurrenceTest(){
    List<String> list = List.of("1","2","1","1","2");
    Assertions.assertEquals(3, listMethods.countOccurrence(list,"1"));
  }
  @Test
  public void toListTest(){
     int[] array = new int[]{1,4,5,2,1};
     List<Integer> newList = listMethods.toList(array);
     List<Integer> expectedList = List.of(1,4,5,2,1);
     Assertions.assertEquals(expectedList,newList);
   }
   @Test
  public void findUniqueTest(){
     List<Integer> numList = List.of(3,5,12,5,3,4,3,1,2);
     List<Integer> expectedList = List.of(3,5,12,4,1,2);
     Assertions.assertEquals(expectedList,listMethods.findUnique(numList));
   }
   @Test
  public void calcOccurrenceTest(){
     List<String> stringList = List.of("dog","cat","dog","horse","dog","horse","bird","bird","fox","fox","dog","mouse","mouse");
     listMethods.calcOccurrence(stringList);
   }
   @Test
  public void findOccurrenceTest(){
     List<String> stringList = List.of("dog","cat","dog","horse","dog","horse","bird","bird","fox","fox","dog","mouse","mouse");
     List<Counter> actualList = listMethods.findOccurrence(stringList);
     List<Counter> expectedList = new ArrayList<>();
     expectedList.add(new Counter("horse",2));
     expectedList.add(new Counter("mouse",2));
     expectedList.add(new Counter("cat",1));
     expectedList.add(new Counter("bird",2));
     expectedList.add(new Counter("dog",4));
     expectedList.add(new Counter("fox",2));
     for (int i=0 ;i<actualList.size();i++){
       Counter expectedCounter = expectedList.get(i);
       Counter actualCounter = actualList.get(i);
       Assertions.assertEquals(expectedCounter.getName(),actualCounter.getName());
       Assertions.assertEquals(expectedCounter.getOccurrence(),actualCounter.getOccurrence());
     }
   }
  }
