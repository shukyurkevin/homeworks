package org.kevin.phonebook;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PhoneDirectoryTest {
  PhoneDirectory phoneDirectory = new PhoneDirectory();
  @BeforeEach
  public void setUp(){
    phoneDirectory.add("Kevin",1313214124);
    phoneDirectory.add("Yuki",1313217624);
    phoneDirectory.add("Emile",1343211224);
    phoneDirectory.add("Kevin",1313212324);
    phoneDirectory.add("Dima",1313214125);
  }
  @Test
  public void findTest(){
    Record expectedRecord = new Record("Kevin",1313214124);
    Assertions.assertEquals(expectedRecord.getName(),phoneDirectory.find("Kevin").getName());
    Assertions.assertEquals(expectedRecord.getPhoneNumber(),phoneDirectory.find("Kevin").getPhoneNumber());
  }
  @Test
  public void findAllTest(){
    List<Record> expectedList = new ArrayList<>();
    expectedList.add(new Record("Kevin",1313214124));
    expectedList.add(new Record("Kevin",1313212324));
    List<Record> actualList = phoneDirectory.findAll("Kevin");
    Assertions.assertEquals(expectedList.size(),actualList.size());
    for(int i=0; i<actualList.size();i++){
      Record expectedRecord = expectedList.get(i);
      Record actualRecord = actualList.get(i);
      Assertions.assertEquals(expectedRecord.getName(),actualRecord.getName());
      Assertions.assertEquals(expectedRecord.getPhoneNumber(),actualRecord.getPhoneNumber());
    }
  }
}
