package org.kevin;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class Tree {
  private TreeNode rootNode;
@Getter
  public static class TreeNode{
    private List<TreeNode> children;
    private int num;

    public TreeNode(int num){
      this.num = num;
      this.children = new ArrayList<>();

    }
    public void addChildren(TreeNode child){
      children.add(child);
    }
  }

public Tree(int rootNum){
  rootNode = new TreeNode(rootNum);
}
public TreeNode getRootNode(){
  return rootNode;
}
public List<Integer> treeToList(TreeNode node){
  List<Integer> resultList = new ArrayList<>();
  Iteration(resultList,node);
  return resultList;
}
public void Iteration(List<Integer> resultList,TreeNode node){

  if (node == null){
    return;
  }
  resultList.add(node.getNum());
  for (TreeNode child: node.getChildren()){
    Iteration(resultList,child);
  }
}
}
