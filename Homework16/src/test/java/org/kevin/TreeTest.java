package org.kevin;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TreeTest {
  @Test
  public void test(){
    Tree tree = new Tree(1);
    Tree.TreeNode rootNode = tree.getRootNode();
    Tree.TreeNode child1 = new Tree.TreeNode(2);
    Tree.TreeNode child2 = new Tree.TreeNode(3);
    Tree.TreeNode child11 = new Tree.TreeNode(21);
    Tree.TreeNode child21 = new Tree.TreeNode(31);
    Tree.TreeNode child12 = new Tree.TreeNode(22);
    Tree.TreeNode child22 = new Tree.TreeNode(32);
    rootNode.addChildren(child1);
    rootNode.addChildren(child2);
    child1.addChildren(child11);
    child1.addChildren(child12);
    child2.addChildren(child21);
    child2.addChildren(child22);
    List<Integer> expectedList = List.of(1, 2, 21, 22, 3, 31, 32);
    Assertions.assertEquals(expectedList,tree.treeToList(rootNode));

  }
}
