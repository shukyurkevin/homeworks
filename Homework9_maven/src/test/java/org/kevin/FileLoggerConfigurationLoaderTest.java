package org.kevin;

import java.net.URL;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FileLoggerConfigurationLoaderTest {
  private FileLoggerConfigurationLoader fileLoggerConfigurationLoader;
  public FileLoggerConfiguration fileLoggerConfiguration;
  @BeforeEach
  public void setUp(){
    fileLoggerConfigurationLoader = new FileLoggerConfigurationLoader();
    URL pathURL = Main.class.getClassLoader().getResource("Loader.yaml");
    String path = null;
    if (pathURL != null) {
      path = pathURL.getFile();
    }
    fileLoggerConfiguration = fileLoggerConfigurationLoader.load(path);
  }
  @Test
  public void testConstructor(){
    Assertions.assertNotNull(fileLoggerConfiguration);
  }
  @Test
  public void testLevelSetter(){
    Assertions.assertEquals("DEBUG",fileLoggerConfiguration.level.name());
  }
  public void testMaxSizeSet(){
    Assertions.assertEquals(1000,fileLoggerConfiguration.maxSize);
  }
  public void testFormatSet(){
    Assertions.assertEquals(".txt",fileLoggerConfiguration.format);
  }
}
