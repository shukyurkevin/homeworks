package org.kevin;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FileLoggerTest {
  private FileLoggerConfiguration config;
  private FileLogger fileLogger;

  @BeforeEach
  public void setUp() {
    config = new FileLoggerConfiguration();
    config.setPath("src/test/resources/logs/");
    config.setLevel(LogginLevel.DEBUG);
    config.setMaxSize(1000);
    config.setFormat(".txt");
    fileLogger = new FileLogger(config);
  }

  @Test
  public void testDebugLogging() throws IOException {
    String message = "debugTest";
    fileLogger.debug(message);
    assertTrue(fileContentContains(fileLogger.file, message));
  }

  @Test
  public void testInfoLogging() throws IOException {
    String message = "infoTest";
    fileLogger.info(message);
    assertTrue(fileContentContains(fileLogger.file, message));
  }

  private boolean fileContentContains(File file, String content) throws IOException {
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      String line;
      while ((line = reader.readLine()) != null) {
        if (line.contains(content)) {
          return true;
        }
      }
    }
    return false;
  }
}