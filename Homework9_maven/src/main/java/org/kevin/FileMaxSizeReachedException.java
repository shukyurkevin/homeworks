package org.kevin;
public class FileMaxSizeReachedException extends RuntimeException {
  public FileMaxSizeReachedException(String msg) {
    super(msg);
  }
}
