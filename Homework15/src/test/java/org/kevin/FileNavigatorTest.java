package org.kevin;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FileNavigatorTest {
  FileNavigator fileNavigator = new FileNavigator();

  @BeforeEach
  public void setUp() {
    fileNavigator.add(new FileData("Kevin", 400, "src/main/java/resources"),
        "src/main/java/resources");
    fileNavigator.add(new FileData("Emile", 600, "src/main/java/resources"),
        "src/main/java/resources");
    fileNavigator.add(new FileData("Yuki", 300, "src/main/java"), "src/main/java");
  }

  @Test
  public void firstAddTest() {
    Assertions.assertThrows(RuntimeException.class,() -> fileNavigator.add(new FileData("Max", 500, "src/main/java/resources"), "src/main/java"));
  }

  @Test
  public void secondAddTest() {
    Map<String,LinkedList<FileData>> expectedMap = new HashMap<>();
    expectedMap.put("src/main/java/resources", new LinkedList<>(){{
      add(new FileData("Kevin",400,"src/main/java/resources"));
      add(new FileData("Emile", 600, "src/main/java/resources"));
    }});
    expectedMap.put("src/main/java", new LinkedList<>(){{
      add(new FileData("Yuki", 300, "src/main/java"));
      add(new FileData("Max", 500, "src/main/java"));
    }});
    fileNavigator.add(new FileData("Max", 500, "src/main/java"), "src/main/java");
   Assertions.assertEquals(expectedMap,fileNavigator.fileMap);
  }
  @Test
  public void find(){
    LinkedList<FileData> list = fileNavigator.find("src/main/java/resources");
    LinkedList<FileData> expectedList = new LinkedList<>(){{
      add(new FileData("Kevin", 400, "src/main/java/resources"));
      add(new FileData("Emile", 600, "src/main/java/resources"));
    }};
    Assertions.assertEquals(expectedList,list);
  }

  @Test
  public void removeTest() {
    fileNavigator.remove("src/main/java/resources");
    Map<String,LinkedList<FileData>> expectedMap = new HashMap<>();
    expectedMap.put("src/main/java",new LinkedList<>(){{
      add(new FileData("Yuki", 300, "src/main/java"));
    }});
    Assertions.assertEquals(expectedMap,fileNavigator.fileMap);
  }
  @Test
  public void filterTest(){
    LinkedList<FileData> list = fileNavigator.filterBySize(380);
    LinkedList<FileData> expectedList = new LinkedList<>(){{
      add(new FileData("Yuki", 300, "src/main/java"));
    }};
    Assertions.assertEquals(expectedList,list);
  }
  @Test
  public void sortTest(){
    LinkedList<FileData> list = fileNavigator.sortBySize();
    LinkedList<FileData> expectedList = new LinkedList<>(){{
      add(new FileData("Yuki", 300, "src/main/java"));
      add(new FileData("Kevin", 400, "src/main/java/resources"));
      add(new FileData("Emile", 600, "src/main/java/resources"));
    }};
    Assertions.assertEquals(expectedList,list);
  }
}