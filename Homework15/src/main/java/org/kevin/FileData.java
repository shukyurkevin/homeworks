package org.kevin;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Builder
@Getter
@EqualsAndHashCode
public class FileData {
  private String name;
  private int size;
  private String path;
}
