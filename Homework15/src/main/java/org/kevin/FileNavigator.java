package org.kevin;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class FileNavigator {
  public Map<String, LinkedList<FileData>> fileMap = new HashMap<>();
  public void add(FileData file,String path){
    if (!file.getPath().equals(path)){
      throw new RuntimeException("file path is not same as entered path");
    }
    fileMap.computeIfAbsent(path, key -> new LinkedList<>()).add(file);
  }
  public LinkedList<FileData> find(String path){
    return fileMap.getOrDefault(path, new LinkedList<>());
  }
  public LinkedList<FileData> filterBySize(int maxSize){
    LinkedList<FileData> newList = new LinkedList<>();
    for (Map.Entry<String,LinkedList<FileData>> entry: fileMap.entrySet()){
      for (FileData fileData: entry.getValue()){
      if (fileData.getSize() <= maxSize){
        newList.add(fileData);
      }
      }
    }
    return newList;
  }
  public void remove(String path){
    fileMap.remove(path);
  }
  public LinkedList<FileData> sortBySize(){
    LinkedList<FileData> sortedList = new LinkedList<>();
    for (Map.Entry<String,LinkedList<FileData>> entry: fileMap.entrySet()){
      sortedList.addAll(entry.getValue());
    }
    sortedList.sort(Comparator.comparingInt(FileData::getSize));
    return sortedList;
  }
  }
