package org.kevin.fruits;

abstract class Fruit {
  abstract float getWeight();
}