package org.kevin.fruits;

import java.util.ArrayList;
import java.util.List;

public class Box<T extends Fruit>{
  private List<T> fruits;

  public Box(){
    fruits = new ArrayList<>();
  }

  public void add(T fruit){
    fruits.add(fruit);
  }

  public void addMany(T fruit,int count){
    for (int i = 0; i<count; i++){
      fruits.add(fruit);
    }
  }
  public float getWeight() {
    if (fruits.isEmpty())return 0;
    float weight = fruits.get(0).getWeight() ;
    return weight*fruits.size();
  }

  public boolean compare(Box<?> someBox){
    return this.getWeight() > someBox.getWeight();
  }

  public void merge(Box<T> other) {
    this.fruits.addAll(other.fruits);
    other.fruits.clear();
  }
}
