package org.kevin;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ToListTest {
  @Test
  public void test(){
    List<Integer> expectedList = List.of(1,2,3,4,5);
    Integer[] array = new Integer[]{1,2,3,4,5};
    List<Integer> resultList = Main.toList(array);
    Assertions.assertEquals(expectedList,resultList);
  }
}
