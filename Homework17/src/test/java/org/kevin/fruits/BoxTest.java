package org.kevin.fruits;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BoxTest {

  @Test
  public void test(){
    Box<Apple> appleBox = new Box<>();
    appleBox.add(new Apple());
    appleBox.addMany(new Apple(), 5);
    float expectedWeight = 6.0f;
    Assertions.assertEquals(expectedWeight,appleBox.getWeight());
  }

  @Test
  public void test2(){
    Box<Apple> appleBox = new Box<>();
    Box<Orange> orangeBox = new Box<>();
    appleBox.addMany(new Apple(),5);
    orangeBox.add(new Orange());
    orangeBox.addMany(new Orange(),6);
    Assertions.assertTrue(orangeBox.compare(appleBox));
  }

  @Test
  public void test3(){
    Box<Apple> appleBox = new Box<>();
    Box<Apple> appleBox2 = new Box<>();
    appleBox.addMany(new Apple(),3);
    Assertions.assertEquals(3,appleBox.getWeight());
    appleBox2.addMany(new Apple(),3);
    appleBox.merge(appleBox2);
    Assertions.assertEquals(6,appleBox.getWeight());
  }
}
