public class Cat extends Animals{
    public static int catCount = 0;
    public Cat(String name){
        this.name = name;
        animalCount++;
        catCount++;
    }
    @Override
    public void run(int meters) {
        if (meters > 200){
            System.out.println(name+ " не может столько пробежать");
        }else {
            System.out.println(name+" пробежал "+meters+" м.");
        }
    }

    @Override
    public void swim(int meters) {
        System.out.println(name+" не умеет плавать");
    }
}
