public class Main {
    public static void main(String[] args) {
        Cat catMurka = new Cat("Мурка");
        Dog dogBobik = new Dog("Бобик");
        dogBobik.run(400);
        dogBobik.run(600);
        dogBobik.swim(6);
        dogBobik.swim(13);
        System.out.println("--------------------------");
        catMurka.run(200);
        catMurka.run(250);
        catMurka.swim(20);
        System.out.println("--------------------------");
        Cat cat2 = new Cat("Дымка");
        Dog dog2 = new Dog("Барсик");
        System.out.println("Количество животных: "+ Animals.animalCount );
        System.out.println("Количество котов: "+ Cat.catCount);
        System.out.println("Количество собак: "+ Dog.dogCount);

    }
}