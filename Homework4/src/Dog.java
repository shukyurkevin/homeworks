public class Dog extends Animals{
    public static int dogCount = 0;
    public Dog(String name){
        this.name = name;
        animalCount++;
        dogCount++;
    }
    @Override
    public void swim(int meters) {
        if(meters > 10){
            System.out.println(name+" не может столько проплыть");
        }else {
            System.out.println(name+" проплыл "+meters+" м.");
        }
    }

    @Override
    public void run(int meters) {
        if(meters > 500){
            System.out.println(name+" не может столько пробежать");
        }else {
            System.out.println(name+" пробежал "+meters+" м.");
        }
    }

}
