package net.kevin.homework5.p1;

public class Circle implements IArea {
    public double radius;
    public double area = -1;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double countArea() {
        if(area!=-1){
            return area;
        }else{
            area = radius*Math.PI;
            return area ;
        }
    }
}
