package net.kevin.homework5.p1;

public class GeometryMain {
    public static void main(String[] args) {
        double sumArea = 0;
        IArea[] masFigures = new IArea[5];
        masFigures[0] = new Circle(6);
        masFigures[1] = new Circle(4);
        masFigures[2] = new Square(4);
        masFigures[3] = new Triangle(4,7);
        masFigures[4] = new Square(7);
        for (int i = 0;i< masFigures.length;i++){
            System.out.println("Площадь "+(i+1)+" фигуры: "+masFigures[i].countArea());
            sumArea += masFigures[i].countArea();
        }
        System.out.println("---------------------------------------");
        System.out.println("Сумма площадей всех фигур: "+sumArea);

    }
}
