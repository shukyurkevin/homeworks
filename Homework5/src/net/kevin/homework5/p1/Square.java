package net.kevin.homework5.p1;

public class Square implements IArea{
    public double a;
    public double area = -1;

    public Square(double a) {
        this.a = a;
    }

    @Override
    public double countArea() {
        if (area!=-1){
            return area;
        }else {
            area = a * a;
            return area;
        }
    }
}
