package net.kevin.homework5.p1;

public class Triangle implements IArea{
    public double aHeight;
    public double a;
    public double area=-1;

    public Triangle(double aHeight, double a) {
        this.aHeight = aHeight;
        this.a = a;
    }

    @Override
    public double countArea() {
        if (area != -1){
            return area;
        }else {
            area = a * aHeight * 0.5;
            return area;
        }
    }
}
