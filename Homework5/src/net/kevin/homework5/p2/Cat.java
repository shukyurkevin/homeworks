package net.kevin.homework5.p2;

public class Cat extends Participants{

    public Cat(String name,int runlimit,int climblimit){
        this.name = name;
        this.climblimit = climblimit;
        this.runlimit = runlimit;
    }

    @Override
    public void run() {
        super.run();
    }

    @Override
    public void jump() {
        super.jump();
    }
}