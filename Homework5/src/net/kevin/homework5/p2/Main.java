package net.kevin.homework5.p2;

public class Main {
    public static void main(String[] args) {
        Participants[] someParticipants = new Participants[6];
        someParticipants[0] = new Human("normal human",12000,12);
        someParticipants[1] = new Human("sportsman",20000,22);
        someParticipants[2] = new Cat("Cat",9000,20);
        someParticipants[3] = new Cat("home cat",7000,10);
        someParticipants[4] = new Robot("first robot", 100000,1);
        someParticipants[5] = new Robot("second robot",10000,100);
        Obstacle[] someObstacle = new Obstacle[6];
        someObstacle[0] = new Track("first track",8000);
        someObstacle[1] = new Track("second track",14000);;
        someObstacle[2] = new Wall("first wall",10);
        someObstacle[3] = new Wall("second wall",20);
        someObstacle[4] = new Track("third track",25000);
        someObstacle[5] = new Wall("third wall",50);
        for (int i = 0;i<someParticipants.length;i++){
            for (int j = 0;j<someObstacle.length;j++){
                someObstacle[j].overcome(someParticipants[i]);
            }
        }

    }
}