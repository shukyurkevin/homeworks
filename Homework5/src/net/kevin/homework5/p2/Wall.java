package net.kevin.homework5.p2;

public class Wall extends Obstacle{
    public int height;
    public Wall(String name,int height){
        this.height = height;
        this.name = name;
    }
    @Override
    public void overcome(Participants participant) {
        if (!participant.passedClimb){
            return;
        }
        else if (participant.climblimit < height) {
            System.out.println(participant.name + " didn`t pass "+name+", he stoped at: "+ participant.climblimit+" м.");
            participant.passedClimb = false;
        } else {
            System.out.println(participant.name+" passed "+name+" Which was "+ height+" м.");
            participant.passedClimb = true;
        }
    }

}
