package net.kevin.homework5.p2;

public class Participants {
    public String name;
    public int runlimit;
    public int climblimit;
    public boolean passedRun = true;
    public boolean passedClimb = true;
    public void jump(){
        System.out.println(name+" jumping");
    }
    public void run(){
        System.out.println(name +" running");
    }
}
