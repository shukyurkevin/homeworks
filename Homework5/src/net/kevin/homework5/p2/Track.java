package net.kevin.homework5.p2;

public class Track extends Obstacle {
    public int lenght;
    public Track(String name,int lenght){
        this.lenght = lenght;
        this.name = name;
    }

    @Override
    public void overcome(Participants participant) {
        if (!participant.passedRun) {
            return;
        }else if (participant.runlimit < lenght) {
            System.out.println(participant.name + " didn`t passed "+name+",passed: "+ participant.runlimit+"м.");
            participant.passedRun = false;
        } else {
            System.out.println(participant.name+" passed "+name+" which was "+ lenght+" м.");
        participant.passedRun = true;
        }
    }
}