package net.kevin.homework5.p2;

public class Robot extends Participants{

    public Robot(String name,int runlimit,int climblimit){
        this.name = name;
        this.climblimit = climblimit;
        this.runlimit = runlimit;
    }

    @Override
    public void jump() {
        super.jump();
    }

    @Override
    public void run() {
        super.run();
    }
}
