public class HomeWorkApp {
    public static void main(String[] args) {
        HomeWorkApp obj = new HomeWorkApp();
        System.out.println("2------------------");
        obj.printThreeWords();
        System.out.println("3------------------");
        obj.checkSumSign(5, 4);
        obj.checkSumSign(9, -10);
        System.out.println("4------------------");
        obj.printColor(0);
        obj.printColor(100);
        obj.printColor(101);
        System.out.println("5------------------");
        obj.compareNumbers(7, 6);
        obj.compareNumbers(-20, 10);
        System.out.println("6------------------");
        System.out.println(obj.sumBetween10_20(5,6));
        System.out.println(obj.sumBetween10_20(10,11));
        System.out.println("7------------------");
        obj.isPositiveNumber(-10);
        obj.isPositiveNumber(10);
        System.out.println("8------------------");
        System.out.println(obj.isNegativeNumber(5));
        System.out.println(obj.isNegativeNumber(-2));
        System.out.println("9------------------");
        obj.printString("Hello",3);
        System.out.println("10-----------------");
        System.out.println(obj.isLeapYear(2000));
        System.out.println(obj.isLeapYear(1000));
        System.out.println(obj.isLeapYear(1004));
        System.out.println(obj.isLeapYear(2003));

    }
    public void printThreeWords(){
        System.out.println("Orange");
        System.out.println("Banana");
        System.out.println("Apple");
    }
    public void checkSumSign(int a,int b){
        if(a+b >= 0){
            System.out.println("the sum is positive");
        }else {
            System.out.println("the sum is negative");
        }
    }
    public void printColor(int value){
        if (value>100){
            System.out.println("Green");
        }else if (value>0){
            System.out.println("Yellow");
        }else {
            System.out.println("Red");
        }
    }
    public void compareNumbers(int a,int b){
        if (a>=b){
            System.out.println("a >= b");
        }else {
            System.out.println("a < b");
        }
    }
    public boolean sumBetween10_20(int a,int b){
        int sum;
        sum = a+b;
        return sum >= 10 && sum <= 20;
    }
    public void isPositiveNumber(int a){
        if (a<0){
            System.out.println("is negative number");
        }else {
            System.out.println("is positive number");
        }
    }
    public boolean isNegativeNumber(int a){
        return a < 0;
    }
    public void printString(String s,int i){
        for (int j = 0; j<i ;j++){
            System.out.println(s);
        }
    }
    public boolean isLeapYear(int year){
        if (year % 400 == 0){
            return true;
        }
        if (year % 100 == 0){
            return false;
        }
        return year % 4 == 0;
    }
}
