package org.kevin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArrayAfterFourTest {
  @Test
  public void firstArrayAfterFourTest(){
    int[] array = {1,2,3,5,6,7};
   Assertions.assertThrows(RuntimeException.class, () -> Main.arrayAfterFour(array));
  }
  @Test
  public void secondArrayAfterFourTest(){
    int[] array = {1,4,2,5,6};
    int[] expectedNewArray = {2,5,6};
    Assertions.assertArrayEquals(expectedNewArray,Main.arrayAfterFour(array));
  }
  @Test
  public void ThirdArrayAfterFourTest(){
    int[] array = {1,5,4,2,4,4,2};
    int[] expectedNewArray = {2};
    Assertions.assertArrayEquals(expectedNewArray,Main.arrayAfterFour(array));
  }
}
