package org.kevin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArrayOnlyFourAndOneTest {
  @Test
  public void onlyFourArrayTest(){
    int[] testArray = {4,4,4,4,4,4};
    Assertions.assertFalse(Main.arrayOnlyFourAndOne(testArray));
  }
  @Test
  public void onlyOneArrayTest(){
    int[] testArray = {1,1,1,1,1,1,1,1};
    Assertions.assertFalse(Main.arrayOnlyFourAndOne(testArray));
  }
  @Test
  public void goodArrayTest(){
    int[] testArray = {1,4,4,4,4,1,4,1};
    Assertions.assertTrue(Main.arrayOnlyFourAndOne(testArray));
  }
  @Test
  public void notOnlyFourAndOneArrayTest(){
    int[] testArray = {1,3,4,1,4,1,4};
    Assertions.assertFalse(Main.arrayOnlyFourAndOne(testArray));
  }
}
