package org.kevin;

import jakarta.jws.WebService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@WebService(endpointInterface = "org.kevin.OrderRepositoryService")
public class OrderRepository implements  OrderRepositoryService{
  private final Map<Integer,Order> orders = new HashMap<>();

  public Order getById(int id){
    return orders.get(id);
  }

  public List<Order> getAll(){
    return orders.values().stream().toList();
  }

  public void add(Order order){
    orders.put(order.getId(),order);
  }

}
