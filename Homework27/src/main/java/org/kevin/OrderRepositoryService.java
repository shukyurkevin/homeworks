package org.kevin;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import java.util.List;

@WebService
public interface OrderRepositoryService {

  @WebMethod
  void add(@WebParam(name = "order") Order order);

  @WebMethod
  List<Order> getAll();

  @WebMethod
  Order getById(@WebParam(name = "id") int id);
}