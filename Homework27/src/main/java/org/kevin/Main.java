package org.kevin;


import jakarta.xml.ws.Endpoint;
import lombok.SneakyThrows;

public class Main {
  private static final String ADDRESS = "http://localhost:9999/Orders?wsdl";
  private static final OrderRepositoryService Service = new OrderRepository();
  public static void main(String[] args) {
    Endpoint endpoint = Endpoint.publish(ADDRESS,Service);
    sleep();

    endpoint.stop();
  }

 @SneakyThrows
 private static void sleep(){
    while (true){
      Thread.sleep(100000);
    }
 }
}
