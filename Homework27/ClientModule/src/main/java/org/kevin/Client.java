package org.kevin;

import jakarta.xml.ws.Service;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.namespace.QName;
import lombok.SneakyThrows;

public class Client {
  @SneakyThrows
  public static void main(String[] args) {
    System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
    System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
    System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
    System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
    System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");
    URL url = new URL("http://localhost:9999/Orders?wsdl");
    QName qName = new QName("http://kevin.org/", "OrderRepositoryService");
    Service service = Service.create(url, qName);
    OrderRepositoryService orderRepositoryService = service.getPort(OrderRepositoryService.class);
    Order order2 = Order.builder()
        .id(2)
        .date(Date.from(Instant.now()))
        .cost(30000.0)
        .products(new ArrayList<>())
        .build();
    orderRepositoryService.add(order2);
    orderRepositoryService.getAll();
  }
}
