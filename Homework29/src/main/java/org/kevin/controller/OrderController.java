package org.kevin.controller;

import java.util.UUID;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.kevin.Order;
import org.kevin.repository.OrderRepository;

@Path("/orders")
public class OrderController {
private final OrderRepository orderRepository = new OrderRepository();
@GET
@Path("/getById")
@Produces(MediaType.APPLICATION_JSON)
public Response getByID(@QueryParam("id")UUID uuid){
  return Response.ok(orderRepository.getById(uuid)).build();
}
@GET
@Path("/getAll")
@Produces(MediaType.APPLICATION_JSON)
public Response getAll(){
  return Response.ok(orderRepository.getOrderMap().values().stream().toList()).build();
}
@GET
@Path("/hello")
@Produces(MediaType.APPLICATION_JSON)
  public Response hello(){
  return Response.ok("hello").build();
}
@POST
@Produces(MediaType.APPLICATION_JSON)
  public Response addOrder(Order order){
  orderRepository.add(order);
  return Response.ok(order).build();

}

}
