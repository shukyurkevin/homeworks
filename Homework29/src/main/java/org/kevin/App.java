package org.kevin;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.kevin.controller.OrderController;

@ApplicationPath("/rest")
public class App extends Application {

  private final Set<Object> single = new HashSet<>();

  public App(){
    single.add(new OrderController());
  }

  @Override
  public Set<Object> getSingletons() {
    return single;
  }
}