package org.kevin.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import org.kevin.Order;

@Setter
@Getter
public class OrderRepository {
  private Map<UUID, Order> orderMap;
  public OrderRepository(){
    orderMap = new HashMap<>();
  }
  public Order getById(UUID uuid){
    return orderMap.get(uuid);
  }

  public List<Order> getAll(){
    return orderMap.values().stream().toList();
  }

  public void add(Order order){
    order.setUuid(UUID.randomUUID());
    orderMap.put(order.getUuid(),order);
  }

}
