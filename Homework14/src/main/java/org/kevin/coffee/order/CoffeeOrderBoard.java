package org.kevin.coffee.order;

import java.util.LinkedList;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CoffeeOrderBoard {
  private static final Logger log = LogManager.getLogger(CoffeeOrderBoard.class);
  public List<Order> orderList = new LinkedList<>();
  int orderNum = 1;
  public void add(String name){
    orderList.add(new Order(name, orderNum++));
    log.info(name+" was added");

  }
  public void delivery(){
    orderList.remove(0);
    log.info(orderList.get(0)+" was delivered");
  }
  public void delivery(int orderNum){
    for (Order order: orderList){
      if (order.getNum()== orderNum){
        orderList.remove(order);
        log.info("order №"+orderNum+" was delivered");
        break;
      }
    }
  }
  public void draw(){
    for (Order order: orderList){
      System.out.println(order.getNum()+" | "+order.getName());
    }
    log.info("draw finished");
  }
  @Builder
  @Getter
  private static class Order{
    String name;
    int num;

    @Override
    public String toString() {
      return num + " | " + name;
    }
  }
}
