package org.kevin.coffeeOrderBoard;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kevin.coffee.order.CoffeeOrderBoard;

public class CoffeeOrderBoardTest {
  CoffeeOrderBoard coffeeOrderBoard = new CoffeeOrderBoard();
  @BeforeEach
  public void setUp(){
    coffeeOrderBoard.add("Kevin");
    coffeeOrderBoard.add("Emile");
    coffeeOrderBoard.add("Yuki");
    coffeeOrderBoard.add("Timur");
  }
  @Test
  public void firstTest(){
    Assertions.assertEquals("[1 | Kevin, 2 | Emile, 3 | Yuki, 4 | Timur]",coffeeOrderBoard.orderList.toString());
  }
  @Test
  public void secondTest(){
    coffeeOrderBoard.delivery();
    Assertions.assertEquals("[2 | Emile, 3 | Yuki, 4 | Timur]",coffeeOrderBoard.orderList.toString());

  }
  @Test
  public void thirdTest(){
    coffeeOrderBoard.delivery(3);
    Assertions.assertEquals("[1 | Kevin, 2 | Emile, 4 | Timur]",coffeeOrderBoard.orderList.toString());

  }

}
