package org.kevin.service;

import jakarta.transaction.Transactional;
import org.kevin.entity.Order;
import org.kevin.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
      this.orderRepository = orderRepository;
    }

    @Transactional
    public List<Order> getAllOrders(){

      return orderRepository.findAll();

    }

    @Transactional
    public Optional<Order> getById(Long id){

      return orderRepository.findById(id);

    }
    @Transactional
    public Order addOrder(Order order){

      return orderRepository.save(order);

    }

    @Transactional
    public void deleteOrder(Long id){

      orderRepository.deleteById(id);

    }
}
