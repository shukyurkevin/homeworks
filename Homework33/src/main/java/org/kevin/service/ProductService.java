package org.kevin.service;

import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import org.kevin.entity.Product;
import org.kevin.repository.ProductRepository;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
  private final ProductRepository productRepository;

  public ProductService(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  @Transactional
  public List<Product> getAllOrders(){

    return productRepository.findAll();

  }

  @Transactional
  public Optional<Product> getById(Long id){

    return productRepository.findById(id);

  }
  @Transactional
  public Product addOrder(Product product){

    return productRepository.save(product);

  }

  @Transactional
  public void deleteOrder(Long id){

    productRepository.deleteById(id);

  }

}
