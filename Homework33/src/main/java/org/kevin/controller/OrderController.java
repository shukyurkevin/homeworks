package org.kevin.controller;

import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import org.kevin.entity.Order;
import org.kevin.repository.OrderRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("orders")
public class OrderController {

  private OrderRepository orderRepository;

  public OrderController(OrderRepository orderRepository) {

    this.orderRepository = orderRepository;

  }

  @GetMapping("/{id}")
  public Optional<Order> getById(@PathVariable Long id){

    return orderRepository.findById(id);

  }
  @GetMapping("/getAll")
  public List<Order> getAllOrders() {
    return orderRepository.findAll();
  }

  @PostMapping("/add")
  public void addOrder(@RequestBody Order order) {
    orderRepository.save(order);
  }

  @PostMapping("/delete")
  public void deleteOrder(@RequestParam Long id){
    orderRepository.deleteById(id);
  }
}
