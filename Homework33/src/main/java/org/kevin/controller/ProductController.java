package org.kevin.controller;

import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import org.kevin.entity.Order;
import org.kevin.entity.Product;
import org.kevin.repository.OrderRepository;
import org.kevin.repository.ProductRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("products")
public class ProductController {

  private ProductRepository productRepository;

  public ProductController(ProductRepository productRepository) {

    this.productRepository = productRepository;

  }

  @GetMapping("/{id}")
  public Optional<Product> getById(@PathVariable Long id){

    return productRepository.findById(id);

  }
  @GetMapping("/getAll")
  public List<Product> getAllProducts() {
    return productRepository.findAll();
  }

  @PostMapping("/add")
  public void addOrder(@RequestBody Product product) {
    productRepository.save(product);
  }

}
