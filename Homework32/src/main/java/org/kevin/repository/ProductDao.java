package org.kevin.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.kevin.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDao {

  private final JdbcTemplate jdbcTemplate;

  @Autowired
  public ProductDao(JdbcTemplate jdbcTemplate){
    this.jdbcTemplate = jdbcTemplate;
  }

  public List<?> getAll(){

    String sql = "SELECT * FROM products";

    return jdbcTemplate.query(sql, new ProductMapper());
  }
  public Product getById(Long id) {
    String sql = "SELECT * FROM products WHERE id = ?";
    return jdbcTemplate.queryForObject(sql,new  ProductMapper(),id);
  }

  public void save(Product product) {
    String sql = "INSERT INTO products (name, price) VALUES (?, ?)";
    jdbcTemplate.update(sql, product.getName(), product.getPrice());
  }

  public void update(Product product) {
    String sql = "UPDATE products SET name = ?, price = ? WHERE id = ?";
    jdbcTemplate.update(sql, product.getName(), product.getPrice(), product.getId());
  }

  public void delete(Long id) {
    String sql = "DELETE FROM products WHERE id = ?";
    jdbcTemplate.update(sql, id);
  }

  private static class ProductMapper implements RowMapper<Product>{

    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {

      Product product = new Product();
      product.setId(rs.getLong("id"));
      product.setName(rs.getString("name"));
      product.setPrice(rs.getDouble("price"));

      return product;
    }
  }


}

