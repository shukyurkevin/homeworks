package org.kevin.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.kevin.entity.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CartDao {

  private final JdbcTemplate jdbcTemplate;

  @Autowired
  public CartDao(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }
  public void addCart(Cart cart) {

    String sql = "INSERT INTO carts (id) VALUES (?)";

    jdbcTemplate.update(sql, cart.getId());
  }
  public void addProduct(Long cartId, Long productId, int quantity) {

    String sql = "INSERT INTO carts_products (cart_id, product_id, quantity) VALUES (?, ?, ?)";

    jdbcTemplate.update(sql, cartId, productId, quantity);
  }

  public void removeProduct(Long cartId, Long productId) {

    String sql = "DELETE FROM carts_products WHERE cart_id = ? AND product_id = ?";

    jdbcTemplate.update(sql, cartId, productId);
  }

  private static class CartMapper implements RowMapper<Cart> {
    @Override
    public Cart mapRow(ResultSet rs, int rowNum) throws SQLException {

      Cart cart = new Cart();
      cart.setId(rs.getLong("id"));

      return cart;
    }
  }
}