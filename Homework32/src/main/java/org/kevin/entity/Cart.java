package org.kevin.entity;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Cart {
  private Long id;
  private List<Product> productList;
}
