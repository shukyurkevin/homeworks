package config;

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan(basePackages = "org.kevin")
public class AppConfig {

  @Bean
  public DataSource dataSource(){

    DriverManagerDataSource dataSource = new DriverManagerDataSource();

    dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
    dataSource.setUrl("jdbc:mysql://localhost/homework_32");
    dataSource.setUsername("kevin");
    dataSource.setPassword("password");

    return dataSource;
  }

  @Bean
  public JdbcTemplate jdbcTemplate(){
    return new JdbcTemplate(dataSource());
  }
}

