import java.util.ArrayList;
import java.util.List;

public class ArrayValueCalculator {
    public int doCalc(String[][] mass){
        List<String> errors = new ArrayList<String>();
        int sum = 0;
            if (mass.length != 4 || mass[0].length != 4) {
                throw new ArraySizeException("his size is: "+mass.length+"x"+mass[0].length);}
             for (int i=0;i<mass.length; i++) {
                for (int j = 0; j < mass[i].length; j++) {
                    try {
                    sum = sum + Integer.parseInt(mass[i][j]);
                }catch (NumberFormatException ex2){
                        errors.add(i+"-"+j);
        }}
             } if (!errors.isEmpty()){
            throw new ArrayDataException("There is no number in cell: "+errors);
        }
        return sum;
    }
}
