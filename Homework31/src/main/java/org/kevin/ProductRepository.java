package org.kevin;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class ProductRepository {
  private final List<Product> productList = new ArrayList<>();


  public ProductRepository(){

    productList.add(new Product(1,"apple",4.5));
    productList.add(new Product(2, "orange",6.5));
    productList.add(new Product(3,"banana",12));

  }

  public Product getById(int id){
    return productList.stream()
                      .filter(p -> p.getId() == id)
                      .findAny()
                      .orElse(null);
  }

  public List<Product> getAll(){

    return productList;
  }
}

