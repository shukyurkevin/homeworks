package org.kevin;

import java.util.Scanner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
public class App {
  public static void main(String[] args) {
    ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

    ProductRepository productRepository = context.getBean(ProductRepository.class);

    Cart cart = context.getBean(Cart.class);

    Scanner scanner = new Scanner(System.in);

    String answer = "";
    while (!answer.equals("6")) {
      System.out.println("1. Show all Products");
      System.out.println("2. Add to cart");
      System.out.println("3. Remove from cart");
      System.out.println("4. Show cart");
      System.out.println("5. Calculate price");
      System.out.println("6. End shopping");

      answer = scanner.nextLine();

      switch (answer){
        case "1" -> System.out.println(productRepository.getAll());

        case "2" -> {
          System.out.println("Enter product id");
          int id = Integer.parseInt(scanner.nextLine());
          Product product = productRepository.getById(id);
          if (product != null){
            cart.addProduct(product);
            System.out.println("Product added successfully");
          }else {
            System.out.println("There is no product with this id");
          }
        }

        case "3" -> {
          System.out.println("Enter product id");
          int id = Integer.parseInt(scanner.nextLine());
          if (cart.getAll()
              .stream()
              .filter(p -> p.getId() == id)
              .findAny()
              .orElse(null) != null){

              cart.remove(id);
            System.out.println("Product deleted from cart");
          }else {
            System.out.println("There is no product with this id in cart");
          }

        }

        case "4" -> System.out.println("Your cart: " + cart.getAll());

        case "5" -> {double sum = cart.getAll()
            .stream()
            .mapToDouble(Product::getPrice)
            .sum();
          System.out.println("Full price: "+ sum);
        }

        case "6" -> answer = "6";
      }
    }
  }
}