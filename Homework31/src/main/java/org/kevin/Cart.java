package org.kevin;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Cart {

  private final List<Product> productList = new ArrayList<>();

  public void addProduct(Product product){

    productList.add(product);

  }

  public void remove(int id) {
    productList.removeIf(product -> product.getId() == id);
  }

  public List<Product> getAll(){
    return productList;
  }

}
