import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;
public class Main {
    public static void main(String[] args) {
        Main obj = new Main();
        Random rand = new Random();
        Scanner scan = new Scanner(System.in);
        System.out.println("2--------------------");
        System.out.println(obj.findSymbolOccurance("ababac",'a'));
        System.out.println("3--------------------");
        System.out.println(obj.findWordPosition("World","rld"));
        System.out.println(obj.findWordPosition("World","rd"));
        System.out.println("4--------------------");
        System.out.println(obj.stringReverse("dlroW"));
        System.out.println("5--------------------");
        System.out.println(obj.isPalindrome("ababa"));
        System.out.println(obj.isPalindrome("baaba"));
        System.out.println("6--------------------");
        String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado" , "broccoli", "carrot",
                "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive",
                "pea", "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};
        String correctAnswer = words[rand.nextInt(words.length)];
        char[] showAnswer = new char[15];
        Arrays.fill(showAnswer, '#');
        String userAnswer = null;
        System.out.println("Try to guess!");
        while (true){
            System.out.println();
            userAnswer = scan.nextLine();
            if (userAnswer.equals(correctAnswer)){
                System.out.println("You Win!");
                System.out.println("Word was: "+correctAnswer);
                break;
            }
            for (int i = 0;i<correctAnswer.length()&&i<userAnswer.length();i++){
                   if (correctAnswer.charAt(i) == userAnswer.charAt(i)){
                       showAnswer[i] = userAnswer.charAt(i);
                   }
                }
                for (int i = 0;i<showAnswer.length;i++){
                    System.out.print(showAnswer[i]);
                }
                System.out.println("Try again");
            }
        }

    public int findSymbolOccurance(String string,char a){
        int sum = 0;
        for (char c : string.toCharArray()) {
            if (c == a)sum++;
        }
        return sum;
    }
    public int findWordPosition(String source,String target){
        return source.indexOf(target);
    }
    public String stringReverse(String s){
        return new StringBuilder(s).reverse().toString();
    }
    public boolean isPalindrome(String s){
        String s1 = new StringBuilder(s).reverse().toString();
        return s.equals(s1);
    }
}