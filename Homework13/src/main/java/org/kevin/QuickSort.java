package org.kevin;

public class QuickSort {
  public static void quickSort(int[] arr, int from, int to) {
    if (from < to) {
      int pivotIndex = partition(arr, from, to);
      quickSort(arr, from, pivotIndex - 1);
      quickSort(arr, pivotIndex + 1, to);
    }
  }

  public static int partition(int[] arr, int from, int to) {
    int pivot = arr[to];
    int i = from - 1;

    for (int j = from; j < to; j++) {
      if (arr[j] < pivot) {
        i++;
        swap(arr, i, j);
      }
    }

    swap(arr, i + 1, to);

    return i + 1;
  }

  public static void swap(int[] arr, int i, int j) {
    int temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }
}