package org.kevin;

public class ShakerSort {

  public static void shakerSort(int[] array){
    boolean isNotSorted = true;
    int start = 0;
    int end = array.length;
    while (isNotSorted){
      isNotSorted = false;
      for(int i = start;i<end-1;i++){
        if (array[i] > array[i+1]){
          swap(array,i,i+1);
          isNotSorted = true;
        }
      }
      if (!isNotSorted){
        break;
      }
      isNotSorted = false;
      end--;
      for (int i = end-1; i>=start; i--){
        if(array[i] > array[i+1]){
          swap(array,i,i+1);
          isNotSorted = true;
        }
      }
      start++;
    }
  }
  public static void swap(int[] array, int a,int b){
    int temp = array[a];
    array[a] = array[b];
    array[b] = temp;
  }
}
