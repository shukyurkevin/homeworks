package org.kevin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QuickSortTest {
  @Test
  public void quickSortTest(){
    int[] array = {4,6,9,4,6,7,8,2};
    int[] expectedArray = {2,4,4,6,6,7,8,9};
    QuickSort.quickSort(array,0,array.length-1);
    Assertions.assertArrayEquals(expectedArray,array);
  }

}
