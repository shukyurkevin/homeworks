package org.kevin;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ShakerSortTest {
  @Test
  public void shakerSortTest() {
    int[] array = {10, 7, 8, 9, 1, 5, 4, 5, 9, 12};
    int[] expectedArray = {1, 4, 5, 5, 7, 8, 9, 9, 10, 12};
    ShakerSort.shakerSort(array);
    Assertions.assertArrayEquals(expectedArray,array);

  }
}
