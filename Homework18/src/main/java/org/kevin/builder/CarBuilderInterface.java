package org.kevin.builder;

public interface CarBuilderInterface {
  CarBuilderInterface setEngine(String engine);
  CarBuilderInterface setDoor(String door);
  CarBuilderInterface setWheels(String wheels);
  Car build();
}
