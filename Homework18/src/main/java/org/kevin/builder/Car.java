package org.kevin.builder;

import lombok.Setter;

@Setter
public class Car {
  private String engine;
  private String door;
  private String wheel;
  public String describe(){
    String description;
    description = "Car with "+engine+" engine type"+" , "+door+" and "+wheel;
    return description;
  }

}
