package org.kevin.builder;

public class CarBuilder implements CarBuilderInterface{
  private Car car;
  public CarBuilder(){
    car = new Car();
  }
  @Override
  public CarBuilder setEngine(String engine) {
    car.setEngine(engine);
    return this;
  }

  @Override
  public CarBuilder setDoor(String door) {
    car.setDoor(door);
    return this;
  }

  @Override
  public CarBuilder setWheels(String wheels) {
    car.setWheel(wheels);
    return this;
  }

  @Override
  public Car build() {
    return car;
  }
}
