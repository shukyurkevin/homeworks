package org.kevin.fabric;

class FurnitureFactory {
  static Furniture createFurniture(String type) {
    if (type.equalsIgnoreCase("chair")){
      return new Chair();
    }
    if (type.equalsIgnoreCase("sofa")) {
      return new Sofa();
    }
    if (type.equalsIgnoreCase("table")) {
      return new Table();
    }
    System.out.println("there is no furniture with this type");
    return null;
  }
}
