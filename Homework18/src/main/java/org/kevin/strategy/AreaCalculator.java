package org.kevin.strategy;

public class AreaCalculator {
  public double calculateArea(Figure figure){
    return figure.calcArea();
  }
}
