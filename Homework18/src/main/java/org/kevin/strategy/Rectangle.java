package org.kevin.strategy;

public class Rectangle implements Figure{
  private double firstSide;
  private double secondSide;

  public Rectangle(double firstSide,double secondSide){
    this.firstSide = firstSide;
    this.secondSide = secondSide;
  }
  @Override
  public double calcArea() {
    return firstSide*secondSide;
  }
}
