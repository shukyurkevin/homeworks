package org.kevin.strategy;

public class Triangle implements Figure{
  private double height;
  private double side;

  public Triangle(double height,double side){
    this.height = height;
    this.side = height;
  }
  @Override
  public double calcArea() {
    return side*height*0.5 ;
  }
}
