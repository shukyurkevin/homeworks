package org.kevin.strategy;

public interface Figure {
  double calcArea();
}
