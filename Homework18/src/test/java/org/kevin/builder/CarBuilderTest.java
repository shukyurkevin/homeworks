package org.kevin.builder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CarBuilderTest {
  CarBuilder carBuilder = new CarBuilder();
  @Test
  public void test1(){
    carBuilder.setEngine("diesel");
    carBuilder.setDoor("gold doors");
    carBuilder.setWheels("golden wheels");
    Car car = carBuilder.build();
    Assertions.assertEquals("Car with diesel engine type , gold doors and golden wheels",car.describe());
  }
}
