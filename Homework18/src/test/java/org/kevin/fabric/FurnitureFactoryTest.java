package org.kevin.fabric;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FurnitureFactoryTest {
  @Test
  public void test(){
    Furniture table = FurnitureFactory.createFurniture("table");
    Furniture chair = FurnitureFactory.createFurniture("chair");
    Furniture sofa = FurnitureFactory.createFurniture("sofa");
    Assertions.assertEquals(Table.class,table.getClass());
    Assertions.assertEquals(Chair.class,chair.getClass());
    Assertions.assertEquals(Sofa.class,sofa.getClass());
  }
}
