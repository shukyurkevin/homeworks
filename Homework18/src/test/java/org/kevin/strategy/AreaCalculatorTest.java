package org.kevin.strategy;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AreaCalculatorTest {
  @Test
  public void test(){
    Triangle triangle = new Triangle(5,7);
    Rectangle rectangle = new Rectangle(9,4.5);
    AreaCalculator calculator = new AreaCalculator();
    Assertions.assertEquals(12.5,calculator.calculateArea(triangle));
    Assertions.assertEquals(40.5,calculator.calculateArea(rectangle));
  }
}
